#define MAX_NOME 50

//sdasdasdsa

typedef struct terreno{
	int coordenada_x
	int coordenada_y
	char terreno;
	struct terreno* terreno_prox;
}terreno;

typedef struct planeta {
	char nome [MAX_NOME];
	int tamanho_x;
	int tamanho_y;
	struct planeta* planeta_prox;
}planeta;


typedef struct universo {
	int n_planetas;
	planeta* primeiro;

}universo;

void CriarUniverso(universo* universo);
void InicializaUniverso(universo* universo, int n_planetas, planeta* primeiro_planeta);
void ImprimeUniverso(universo* universo);

void InicializaPlaneta(planeta* planeta, char* nome, int x, int y);
void ConcatenaPlaneta(planeta* anterior_planeta, planeta* proximo_planeta);
void ImprimePlaneta(planeta* planeta);
int TerrenoLivre(planeta* planeta, int x, int y);
