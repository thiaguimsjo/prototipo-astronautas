#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "planeta.h"
/* run this program using the console pauser or add your own getch, system("pause") or input loop */

void Introducao(){
	
	char caracter;
	
    printf("Bem vindo ao prototipo do planeta BB =)\n");
	printf("              _-o#&&*''''?d:>b\\_\n");
	printf("          _o/\"`''  '',, dMF9MMMMMHo_\n");
	printf("       .o&#'        `\"MbHMMMMMMMMMMMHo.\n");
	printf("     .o"" '         vodM*$&&HMMMMMMMMMM?.\n");
	printf("    ,'              $M&ood,~'`(&##MMMMMMH\\\n");
	printf("   /               ,MMMMMMM#b?#bobMMMMHMMML\n");
	printf("  &              ?MMMMMMMMMMMMMMMMM7MMM$R*Hk\n");
	printf(" ?$.            :MMMMMMMMMMMMMMMMMMM/HMMM|`*L\n");
	printf("|               |MMMMMMMMMMMMMMMMMMMMbMH'   T,\n");
	printf("$H#:            `*MMMMMMMMMMMMMMMMMMMMb#}'  `?\n");
	printf("]MMH#             ""*""""*#MMMMMMMMMMMMM'    -\n");
	printf("MMMMMb_                   |MMMMMMMMMMMP'     :\n");
	printf("HMMMMMMMHo                 `MMMMMMMMMT       .\n");
	printf("?MMMMMMMMP                  9MMMMMMMM}       -\n");
	printf("-?MMMMMMM                  |MMMMMMMMM?,d-    '\n");
	printf(" :|MMMMMM-                 `MMMMMMMT .M|.   :\n");
	printf("  .9MMM[                    &MMMMM*' `'    .\n");
	printf("   :9MMk                    `MMM#\"        -\n");
	printf("     &M}                     `          .-\n");
	printf("      `&.                             .\n");
	printf("        `~,   .                     ./\n");
	printf("            . _                  .-\n");
	printf("              '`--._,dd###pp=""'\n");
   	printf("Aperte Enter para continuar: ");
	scanf("%c", &caracter);
	
   	system("@cls||clear");
   	
	printf("Seu objetivo sera colonizar o misterioso sistema planetario dos Indicadores em \nbusca de recuperar o conhecimento perdido do nebuloso Acordo de Trabalho da \nDitec \n\n");
	printf("Sua primeira missao sera no planeta Alocacao de Horas\n");
	printf("\nBoa sorte corajoso explorador\n");
	printf("        _..._\n");
	printf("      .'     '.      _\n");
	printf("     /    .-\"\"-\\   _/ \\\n");
	printf("   .-|   /:.   |  |   |\n");
	printf("   |  \\  |:.   /.-'-./\n");
	printf("   | .-'-;:__.'    =/\n");
	printf("   .'=  *=|BB   _.='\n");
	printf("  /   _.  |    ;\n");
	printf(" ;-.-'|    \\   |\n");
	printf("/   | \\    _\\  _\\\n");
	printf("\\__/'._;.  ==' ==\\\n");
	printf("         \\    \\   |\n");
	printf("         /    /   /\n");
	printf("         /-._/-._/\n");
	printf("         \\   `\\  \\\n");
	printf("          `-._/._/\n");
   	printf("Aperte Enter para continuar: ");
   	scanf("%c", &caracter);
   	
   	system("@cls||clear");
}

/*void ImprimeInterface(char planeta[MAX_PLANETA_X][MAX_PLANETA_Y], int energia, int h2o, int o2, int acordados, int dormindo){	
	printf("Recursos: Energia %d, H2O %d, O2 %d, Acordados %d, Dormindo %d\n\n", energia, h2o, o2, acordados, dormindo);
	
	int x = 0;
	int y = 0;
	
	while(x < MAX_PLANETA_X){
		while(y < MAX_PLANETA_Y){
			printf ("[%c]", planeta[x][y]);
			y++;
		}
		printf("\n");
		x++;
		y = 0;
	}
	
	printf("\n");
	
}*/

int main(int argc, char *argv[]) {

	int energia = 20;
	int h2o = 0;
	int o2 = 0;
	int acordados = 0;
	int dormindo = 5;
	int n_casas = 0;
	
	int escolha = 0;
	char escolha2 = ' ';
	
	int coordenada_x = -1;
	int coordenada_y = -1;
	
	int sair = 0;
	
	universo Universo;
	planeta Alocacao, Capacitacao;
	
	CriarUniverso(&Universo);
	InicializaPlaneta(&Alocacao, "Alocacao de Horas", 3, 3);
	InicializaPlaneta(&Capacitacao, "Capacitacao", 9, 9);
	InicializaUniverso(&Universo, 2, &Alocacao);
	ConcatenaPlaneta(&Alocacao, &Capacitacao);

	srand(time(NULL));
//	int random = rand() % 100;
	
//	printf("%d\n", random);
	
/*	char planeta_alocacao[3][3];
	char planeta_capacitacao[3][3];
	InicializaPlaneta1(planeta_alocacao);
	InicializaPlaneta2(planeta_capacitacao);*/
	
	Introducao();
	
//	planeta[1][1] = 'B';
	

	
	while (sair != 1){
		//ImprimeInterface(planeta, energia, h2o, o2, acordados, dormindo);
		printf("Escolha uma acao:\n1) Construir\n2) Acordar Civis\n3) Sair\nEscolha: "); //escolhas principais
		scanf("%d", &escolha);
        scanf("%c", &escolha2);

		switch(escolha){		
			case 1://menu principal - construcao 
				printf("O que voce deseja construir ?\n");
				printf("1) Estufa de O2 'E' \n2) Tratamento de Agua 'A' \n3) Casas 'C'\n4) Voltar\nEscolha: ");
				scanf("%d", &escolha);

				switch(escolha){
					case 1:
						printf("Estufa de O2: \nEssa construcao ira gerar 10 de O2 por dia \nCusto: 2 de Energia - Total de energia %d \nConfirma Compra? (s/n): ", energia);
						scanf("%c", &escolha2); //retira caracter do enter
						scanf("%c", &escolha2);

						switch(escolha2){
							case 's':
								if(energia >= 2){
									printf("Onde deseja construir ? \nCoordenada X: ");
									scanf("%d", &coordenada_x);
									printf("Onde deseja construir ? \nCoordenada Y: ");
									scanf("%d", &coordenada_y);
/*									if(TerrenoLivre(coordenada_x - 1, coordenada_y - 1, planeta) == 0){
										planeta[coordenada_x - 1][coordenada_y - 1] = 'E';
										energia = energia - 2;
										o2 = o2 + 10;
									}*/
								}
								else{
									printf("Energia Insuficiente\n");
								}
								break;
								
							case 'n':
								break;
								
							default:
								printf("Escolha Invalida\n");
								break;	
						}
						break;
						
					case 2:
						printf("Tratamento de Agua: \nEssa construcao ira gerar 10 de O2 por dia \nCusto: 2 de Energia - Total de energia %d \nConfirma Compra? (s/n): ", energia);
						scanf("%c", &escolha2); //retira caracter do enter
						scanf("%c", &escolha2);
												
						switch(escolha2){
							case 's':
								if(energia >= 2){
									printf("Onde deseja construir ? \nCoordenada X: ");
									scanf("%d", &coordenada_x);
									printf("Onde deseja construir ? \nCoordenada Y: ");
									scanf("%d", &coordenada_y);
									/*if(TerrenoLivre(coordenada_x - 1, coordenada_y - 1, planeta) == 0){
										planeta[coordenada_x - 1][coordenada_y - 1] = 'A';
										energia = energia - 2;
										h2o = h2o + 10;
									}*/
								}
								else{
									printf("Energia Insuficiente\n");
								}
								break;
								
							case 'n':
								break;
								
							default:
								printf("Escolha Invalida\n");
								break;	
						}
						break;
					
					case 3:
						printf("Casas: \nEssa construcao permite abrigar at� 2 exploradores \nCusto: 1 de Energia - Total de energia %d \nConfirma Compra? (s/n): ", energia);
						scanf("%c", &escolha2);//retira caracter do enter
						scanf("%c", &escolha2);
												
						switch(escolha2){
							case 's':
								if(energia >= 1){
									printf("Onde deseja construir ? \nCoordenada X: ");
									scanf("%d", &coordenada_x);
									printf("Onde deseja construir ? \nCoordenada Y: ");
									scanf("%d", &coordenada_y);
									/*if(TerrenoLivre(coordenada_x - 1, coordenada_y - 1, planeta) == 0){
										planeta[coordenada_x - 1][coordenada_y - 1] = 'C';
										energia = energia - 1;
										n_casas = n_casas + 1;
									}*/
								}
								else{
									printf("Energia Insuficiente\n");
								}
								break;
								
							case 'n':
								break;
								
							default:
								printf("Escolha Invalida\n");
								break;	
						}
						break;	
						
					case 4:
						break;
						
					default:
						printf("Escolha Invalida\n");
						break;	
				}
				break;
			
			case 2:// menu principal - acordar
				if(dormindo != 0){
					if(h2o >= 5){
						if(o2 >= 5){
							if(n_casas * 2 > acordados){
								energia = energia - 1;
								while (n_casas * 2 > acordados && o2 >= 5 && h2o >= 5 && dormindo != 0){
									o2 = o2 - 5;
									h2o = h2o - 5;
									acordados = acordados + 1;
									dormindo = dormindo - 1;
								}
							}
							else printf("Nao existem abrigos o suficiente\n");				
						}
						else printf("recursos naturais insuficientes\n");				
						}
					else printf("recursos insuficientes\n");				
				}
				else printf("nenhum explorador para acordar\n");									
				break;
			
			case 3: 
				sair = 1;
				break;				
				
			default:
				printf("Escolha Invalida\n");
				break;				
		}
		if(dormindo == 0){
			//ImprimeInterface(planeta, energia, h2o, o2, acordados, dormindo);
			printf("parabens vc ganhou o jogo =) \nPressione enter para sair");
			scanf("%c", &escolha2);
			sair = 1;
			
		}
	}
	
return 0;
}
